package subscribers

import "fmt"

type HelloWorldSubscriber struct{}

func (HelloWorldSubscriber) Handler(payload string) {
	fmt.Println("HelloWorldSubscriber: ", payload)
}

func (HelloWorldSubscriber) Topic() string {
	return "hello.world"
}
