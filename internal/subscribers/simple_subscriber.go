package subscribers

import "fmt"

type PaymentApprovedSubscriber struct{}

func (PaymentApprovedSubscriber) Handler(payload string) {
	fmt.Println("PaymentApprovedSubscriber: ", payload)
}

func (PaymentApprovedSubscriber) Topic() string {
	return "payment.approved"
}
