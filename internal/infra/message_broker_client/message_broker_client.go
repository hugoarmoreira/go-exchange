package message_broker_client

import (
	"log"

	"github.com/streadway/amqp"
)

type Subscriber interface {
	Handler(payload string)
	Topic() string
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

type MessageBrokerClient struct {
	conn    *amqp.Connection
	channel *amqp.Channel
}

func NewClient() *MessageBrokerClient {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err, "Failed to connect to message broker")
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	return &MessageBrokerClient{conn, ch}
}

func (client *MessageBrokerClient) Close() {
	client.conn.Close()
	client.channel.Close()
}

func (client *MessageBrokerClient) Subscribe(subscriber Subscriber) {
	err := client.channel.ExchangeDeclare(
		subscriber.Topic(), // Exchange name
		"fanout",           // Exchange type
		true,               // Durable
		false,              // Auto-deleted
		false,              // Internal
		false,              // No-wait
		nil,                // Arguments
	)
	failOnError(err, "Failed to declare an exchange")

	q, err := client.channel.QueueDeclare(
		"",    // Queue name (empty to create a unique queue)
		false, // Durable
		false, // Delete when unused
		true,  // Exclusive (queue will be deleted when connection closes)
		false, // No-wait
		nil,   // Arguments
	)
	failOnError(err, "Failed to declare a queue")

	err = client.channel.QueueBind(
		q.Name,             // Queue name
		"",                 // Routing key (empty for fanout exchange)
		subscriber.Topic(), // Exchange name
		false,              // No-wait
		nil,                // Arguments
	)
	failOnError(err, "Failed to bind a queue")

	msgs, err := client.channel.Consume(
		q.Name, // Queue
		"",     // Consumer
		true,   // Auto Ack
		false,  // Exclusive
		false,  // No-local
		false,  // No-wait
		nil,    // Args
	)
	failOnError(err, "Failed to register a consumer")

	go func() {
		for d := range msgs {
			log.Printf(" [x] Received %s", d.Body)
			subscriber.Handler(string(d.Body))
		}
	}()
}

func (client *MessageBrokerClient) Publish(topic string, body string) {
	err := client.channel.Publish(
		topic, // Exchange
		"",    // Routing key
		false, // Mandatory
		false, // Immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		},
	)
	failOnError(err, "Failed to publish a message")
	log.Printf(" [x] Sent %s", body)
}
