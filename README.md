# go-exchange
A custom implementation for handling subscription with amqp message-brokers.

## How it works
The logic behind this implementation starts with the [message-broker-client](https://gitlab.com/hugoarmoreira/go-exchange/-/blob/main/internal/infra/message_broker_client/message_broker_client.go?ref_type=heads). This file is responsible for handling all sorts of logic when it comes to processing message-broker tasks such as publishing a message or subscribing to a topic.

### Subscriber
The client expects to receive a struct that satisfies the `Subscriber` interface. The subscriber must declare a `topic` and a `handler` for the data sent to the declared topic.

The subscribers are declared at the `/internal/subscribers` folder as members from the subscribers package.

Once declared, they can be subscribed in the [entrypoint](https://gitlab.com/hugoarmoreira/go-exchange/-/blob/main/cmd/worker/worker.go?ref_type=heads) of the subscriptions-worker.

## How to run

1. Start the local message-broker, in this case we are using rabbitmq:
```
docker compose up rabbitmq
```

2. On your terminal, start the subscriptions-worker:
```
go run cmd/worker/worker.go
```

3. On a separated terminal, run the [entrypoint for publising a message](https://gitlab.com/hugoarmoreira/go-exchange/-/blob/main/cmd/send_message/send_message.go?ref_type=heads) to one of the subscribers:
```
go run cmd/send_message/send_message.go
```