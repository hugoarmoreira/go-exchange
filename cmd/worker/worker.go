package main

import (
	"go_try/internal/infra/message_broker_client"
	"go_try/internal/subscribers"
	"log"
)

func loop() {
	forever := make(chan bool)

	<-forever
}

func main() {
	client := message_broker_client.NewClient()
	defer client.Close()

	client.Subscribe(subscribers.PaymentApprovedSubscriber{})
	client.Subscribe(subscribers.HelloWorldSubscriber{})

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	loop()
}
