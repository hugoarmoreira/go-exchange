package main

import (
	"go_try/internal/infra/message_broker_client"
)

func main() {
	client := message_broker_client.NewClient()

	client.Publish("payment.approved", "Hello dear friend!")
}
